# HSA L22 Continuous Deployment

 - Setup CI/CD for your pet project
 
##Stage to setup:

### S3
 - Create S3 bucket, 
 - Make it public
 - Enable Static website hosting
 - Add bucket policy%
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicRead",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::__YOUR_BUCKET__/*"
        }
    ]
}
```
### IAM User
- Create user With Access key - Programmatic access
- Store AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
- Add Role for S3 Access

### Gitlab
- Create repo
- Add CI/CD Variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY
  ![Variables](./screenshots/variables.png)
- Write .gitlab-ci.yml
```
stages:
  - build
  - deploy

build website:
  image: node:16-alpine
  stage: build
  script:
    - yarn install
    - yarn lint
    - yarn test
    - yarn build
  artifacts:
    paths:
      - build

deploy to s3:
  stage: deploy
  image:
    name: amazon/aws-cli:2.4.11
    entrypoint: [""]
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
  script:
    - aws --version
    - aws s3 sync build s3://$AWS_S3_BUCKET --delete
```
### Results
Pipelines
![Pipelines](./screenshots/pipelines.png)
Build Stage
![Build](./screenshots/build.png)
Deploy Stage
![Deploy](./screenshots/deploy.png)
S3
![S3](./screenshots/s3.png)
Result
![Result](./screenshots/result.png)